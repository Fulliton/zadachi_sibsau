'use strict'
/* eslint no-eval: 0 */
export class Finder {
  constructor () {
    this.size = {}
    this.start = {}
    this.end = {}
    this.distance = 0
    this.open = {}
    this.close = {}
    this.curr = {}
    this.min = 0
    this.timeoutId = null
  }
  genArea (posX, posY) {
    this.size = {x: posX, y: posY}
    for (let y = 1; y <= posY; y++) {
      for (let x = 1; x <= posX; x++) {
        $('#area').append('<div class="block" id="pos_' + x + '_' + y + '"></div>')
        if (y === 1 || x === 1 || x === posX || y === posY) {
          if (x === 1 && y === 5) {
            $('#pos_' + x + '_' + y).addClass('vxod')
          } else this.addBarrier(x, y)
        }
      }
      $('#area').append('<div class="clear"></div>')
    }
  }

  clearF () {
    let foo = $('#arena')
    foo.detach()
    foo.appendTo('.col-md-8')
  }

  setStart (posX, posY) {
    this.start = {x: posX, y: posY}
    $('#pos_' + posX + '_' + posY).addClass('curr')
    this.curr = this.start
  }

  setEnd (posX, posY) {
    this.end = {x: posX, y: posY}
    $('#pos_' + posX + '_' + posY).addClass('end')
  }

  calcDistance (startX, startY, endX, endY) {
    return Math.abs(endX - startX) + Math.abs(endY - startY)
  }

  setPos (posX, posY) {
    $('#area div.curr').removeClass('curr')
    $('#pos_' + posX + '_' + posY).addClass('curr')
    this.curr = {x: posX, y: posY}
  }

  addClose (posX, posY) {
    let define = false
    if (this.close[posX] !== undefined) {
      if (this.close[posX][posY] !== undefined) {
        define = true
      }
    }

    if (!define) {
      if (this.close[posX] === undefined) {
        this.close[posX] = []
      }
      this.close[posX][posY] = 0
      $('#pos_' + posX + '_' + posY).removeClass('open')
      $('#pos_' + posX + '_' + posY).addClass('closepf')
    }

    if (this.open[posX] !== undefined) {
      if (this.open[posX][posY] !== undefined) {
        this.open[posX][posY] = undefined
      }
    }
  }

  addOpen (posX, posY, parentX, parentY) {
    let define = false
    if (this.open[posX] !== undefined) {
      if (this.open[posX][posY] !== undefined) {
        define = true
      }
    }

    if (this.close[posX] !== undefined) {
      if (this.close[posX][posY] !== undefined) {
        define = true
      }
    }

    if (!define) {
      if (this.open[posX] === undefined) {
        this.open[posX] = []
      }
      this.open[posX][posY] = {
        x: parentX,
        y: parentY,
        to_end: this.calcDistance(posX, posY, this.end.x, this.end.y),
        to_start: this.calcDistance(posX, posY, this.start.x, this.start.y)
      }
      $('#pos_' + posX + '_' + posY).addClass('open')
    }
  }
  addBarrier (posX, posY) {
    $('#pos_' + posX + '_' + posY).addClass('barrier')
    this.addClose(posX, posY)
  }

  findAround () {
    if (this.min) {
      this.setPos(this.min.x, this.min.y)
    }
    this.addClose(this.curr.x, this.curr.y)

    let minX = eval(this.curr.x) - 1
    let minY = eval(this.curr.y) - 1
    let maxX = eval(this.curr.x) + 1
    let maxY = eval(this.curr.y) + 1

    for (let y = minY; y <= maxY; y++) {
      for (let x = minX; x <= maxX; x++) {
        if (x > 0 && y > 0 && x <= this.size.x && y <= this.size.y) {
          this.addOpen(x, y, this.curr.x, this.curr.y)
        }
      }
    }
    this.min = 0
    if (Object.keys(this.open).length) {
      for (let posX in this.open) {
        if (this.open[posX] !== undefined) {
          for (let posY in this.open[posX]) {
            if (this.open[posX][posY] !== undefined) {
              let f = this.open[posX][posY].to_end
              if (this.curr.x !== posX && this.curr.y !== posY) f = f * 1.5
              // $('#pos_' + posX + '_' + posY).text(f)
              if (this.min) {
                if (f < this.min.amount && this.calcDistance(posX, posY, this.curr.x, this.curr.y) <= 1) {
                  this.min = {x: posX, y: posY, amount: f}
                }
              } else {
                this.min = {x: posX, y: posY, amount: f}
              }
            }
          }
        }
      }
    }
  }

  find () {
    this.distance = this.calcDistance(this.start.x, this.start.y, this.end.x, this.end.y)
    this.addOpen(this.curr.x, this.curr.y, this.curr.x, this.curr.y)
    let self = this
    $('.block').click(function () {
      let pos = $(this).attr('id').split('_')
      self.addBarrier(pos[1], pos[2])
    })

    function finder () {
      self.findAround()
      console.log(self.end)
      if ((Number(self.curr.x) === Number(self.end.x)) && (Number(self.curr.y) === Number(self.end.y))) {
        clearTimeout(self.timeoutId)
      }
    }

    this.timeoutId = setInterval(finder, 500)
  }
}
