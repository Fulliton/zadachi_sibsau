import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Главная',
      m: 'glavnaya',
      component: require('@/components/index').default
    },
    {
      path: '/zadanie1',
      name: 'Сортировка методом пузырька',
      m: 'modal1',
      component: require('@/components/module1/zadanie1').default
    },
    {
      path: '/zadanie2',
      name: 'Сортировка методом шейкера',
      m: 'modal1',
      component: require('@/components/module1/zadanie2').default
    },
    {
      path: '/zadanie3',
      name: 'Сортировка методом вставки',
      m: 'modal1',
      component: require('@/components/module1/zadanie3').default
    },
    {
      path: '/zadanie4',
      name: 'Сортировка вставками бинарным поиском',
      m: 'modal1',
      component: require('@/components/module1/zadanie4').default
    },
    {
      path: '/zadanie5',
      name: 'Проверка списка на пустоту ',
      m: 'modal2_1',
      component: require('@/components/module2/zadanie5').default
    },
    {
      path: '/zadanie6',
      name: 'Удаление всех элементов списка с данным значением',
      m: 'modal2_1',
      component: require('@/components/module2/zadanie6').default
    },
    {
      path: '/zadanie7',
      name: 'Определение, сколько различных значений содержится в списке',
      m: 'modal2_1',
      component: require('@/components/module2/zadanie7').default
    },
    {
      path: '/zadanie8',
      name: 'Добавление элемента в начало списка',
      m: 'modal2',
      component: require('@/components/module2/zadanie8').default
    },
    {
      path: '/zadanie9',
      name: 'Изменение порядка элементов на обратный',
      m: 'modal2_1',
      component: require('@/components/module2/zadanie9').default
    },
    {
      path: '/zadanie10',
      name: 'Проверка списка на пустоту',
      m: 'modal2_2',
      component: require('@/components/module2/zadanie10').default
    },
    {
      path: '/zadanie11',
      name: 'Изменение всех элементов списка с данным значением на новое.',
      m: 'modal2_2',
      component: require('@/components/module2/zadanie11').default
    },
    {
      path: '/zadanie12',
      name: 'Инициализация списка',
      m: 'modal2_2',
      component: require('@/components/module2/zadanie12').default
    },
    {
      path: '/zadanie13',
      name: 'Определение количества элементов списка',
      m: 'modal2_2',
      component: require('@/components/module2/zadanie13').default
    },
    {
      path: '/zadanie14',
      name: 'Поиск наибольшего и наименьшего значений в списке',
      m: 'modal2_2',
      component: require('@/components/module2/zadanie14').default
    },
    {
      path: '/zadanie15',
      name: 'Определение, является ли список симметричным',
      m: 'modal2_2',
      component: require('@/components/module2/zadanie15').default
    },
    {
      path: '/zadanie16',
      name: 'Определение, сколько различных значений содержится в списке ',
      m: 'modal2_2',
      component: require('@/components/module2/zadanie16').default
    },
    {
      path: '/zadanie17',
      name: 'Деревья',
      m: 'modal3',
      component: require('@/components/module3/zadanie17').default
    },
    {
      path: '/zadanie18',
      name: 'Ферзи метод 1',
      m: 'modal3',
      component: require('@/components/module3/zadanie18').default
    },
    {
      path: '/zadanie19',
      name: 'Лабиринт',
      m: 'modal3',
      component: require('@/components/module3/zadanie19').default
    }
  ]
})
