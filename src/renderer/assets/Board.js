'use strict'
export class Board {
  constructor (size) {
    this.size = Number(size)
    this.board = new Array(this.size)
  }

  reset () {
    for (let i = 0; i < this.size; i++) {
      this.board[i] = new Array(this.size)
      for (let k = 0; k < this.size; k++) {
        this.board[i][k] = 0
      }
    }
  }
}
